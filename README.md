hKiosk RunVideo Daemon
======================

[![MIT license][license-image]][license-link]

Installation
------------

```shell
DESTDIR= make install
```

License
-------

[MIT][license-link]


[license-image]: https://img.shields.io/badge/license-MIT-blue.svg?style=flat-square
[license-link]: LICENSE
