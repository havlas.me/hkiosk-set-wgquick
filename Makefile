.PHONY: install
install:
	install -d $(DESTDIR)/usr/lib/systemd/system
	install -m 644 systemd/hkiosk-restart-wgquick@.service $(DESTDIR)/usr/lib/systemd/system/
	install -m 644 systemd/hkiosk-restart-wgquick@.path $(DESTDIR)/usr/lib/systemd/system/
	install -m 644 systemd/hkiosk-set-wgquick@.service $(DESTDIR)/usr/lib/systemd/system/
	install -m 644 systemd/hkiosk-set-wgquick@.path $(DESTDIR)/usr/lib/systemd/system/
	install -d $(DESTDIR)/usr/libexec
	install -m 755 libexec/hkiosk-set-wgquick $(DESTDIR)/usr/libexec/

.PHONY: uninstall
uninstall:
	-rm $(DESTDIR)/usr/lib/systemd/system/hkiosk-restart-wgquick@.service
	-rm $(DESTDIR)/usr/lib/systemd/system/hkiosk-restart-wgquick@.path
	-rm $(DESTDIR)/usr/lib/systemd/system/hkiosk-set-wgquick@.service
	-rm $(DESTDIR)/usr/lib/systemd/system/hkiosk-set-wgquick@.path
	-rm $(DESTDIR)/usr/libexec/hkiosk-set-wgquick
